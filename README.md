<h1 align="center">filterview</h1>
> vue filterview component

## 效果

![](https://openteam-1256955938.cos.ap-chengdu.myqcloud.com/filterview/filterview-1.1.0.gif)

### 1.3

![8eYhkT.gif](https://s1.ax1x.com/2020/03/12/8eYhkT.gif)

## 安装|Install

```
cnpm install filterview
```

## 示例|Example （详情在/doc/index.html）

```
<template>
  <filter-view :conditions="conditions" @change="changeHandler"></filter-view>
</template>

<script>
  import FilterView from 'filterview'
  export default {
    components: { 
    	'filter-view': FilterView
    },
    data () {
      return {
          params: "",
      // 查询条件
      conditions: [
        {
          label: "项目类型",
          value: "projType",
          isMultiple: true,
          data: [
            { label: "房建工程", value: "FJGC" },
            { label: "市政工程", value: "SZGC" },
            { label: "其他", value: "99" }
          ],
          custom: false,
          type: "date" // datetime(年月日时分秒) | date(年月日)
        },
        {
          label: "招标结束时间",
          value: "endTime",
          isMultiple: false, // 缺省值 false, 只有单选时自定义才生效
          data: [
            { label: "十天后", value: "10" },
            { label: "20天后", value: "20" },
            { label: "三个月后", value: "3_m" }
          ],
          custom: "single", // 缺省值 false ：没有自定义, "single":表示单个，"range"：表示范围，且后一个比前一个大
          type: "date" // datetime(年月日时分秒) | date(年月日) | number(数字)
        },
        {
          label: "招标结11111束时间2",
          value: "endTime2",
          isMultiple: false, // 缺省值 false, 只有单选时自定义才生效
          data: [
            { label: "十天后", value: "10" },
            { label: "20天后", value: "20" },
            { label: "三个月后", value: "3_m" }
          ],
          custom: "range", // 缺省值 false ：没有自定义, "single":表示单个，"range"：表示范围，且后一个比前一个大
          type: "date" // datetime(年月日时分秒) | date(年月日) | number(数字)
        },
        {
          label: "文本测试",
          value: "endTime3",
          isMultiple: false, // 缺省值 false, 只有单选时自定义才生效
          data: [
            { label: "十天后", value: "10" },
            { label: "20天后", value: "20" },
            { label: "三个月后", value: "3_m" }
          ],
          custom: "single", // 缺省值 false ：没有自定义, "single":表示单个，"range"：表示范围，且后一个比前一个大
          type: "string" // datetime(年月日时分秒) | date(年月日) | string | number(数字)
        },
        {
          label: "文本测试2",
          value: "endTime4",
          isMultiple: false, // 缺省值 false, 只有单选时自定义才生效
          data: [
            { label: "十天后", value: "10" },
            { label: "20天后", value: "20" },
            { label: "三个月后", value: "3_m" }
          ],
          custom: "range", // 缺省值 false ：没有自定义, "single":表示单个，"range"：表示范围，且后一个比前一个大
          type: "string" // datetime(年月日时分秒) | date(年月日) | number(数字)
        }
      ]
      }
    },methods: {
        changeHandler(data) {
          this.params = JSON.stringify(data);
        }
      }
  }
</script>
```



## 属性|Props

| Property | Description | Type | Accepted Values | Default |
|-|-|-|-|-|
| conditions | 条件 | Array | - | - |

## License

[MIT](https://github.com/bestvist/vue-clock2/blob/master/LICENSE) 